

import ballerinax/kafka;
import ballerina/log;

const string SASL_URL = "localhost:9093";



kafka:ConsumerConfiguration consumerConfig = {
    groupId: "auth",
    topics: ["Course-Outline"],
auth  {
        mechanism: kafka:AUTH_SASL_PLAIN,
        username: "alice",
        password: "alice@123"
    },
    securityProtocol: kafka:PROTOCOL_SASL_PLAINTEXT
};


listener kafka:Listener kafkaListener = new(SASL_URL, consumerConfig);

service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,
                    kafka:ConsumerRecord[] records) returns error? {
        foreach var consumerRecord in records {
            string value = check string:fromBytes(consumerRecord.value);
            log:printInfo(value);
        }
    }
}

